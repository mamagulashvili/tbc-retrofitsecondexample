package com.example.tbcnews

data class Resource<T>(val status: Status, val data: T? = null, val message: String? = null) {
    enum class Status {
        Success, Error, Loading
    }

    companion object {
        fun <T> success(data: T): Resource<T> {
            return Resource(Status.Success, data)
        }

        fun <T> error(message: String): Resource<T> {
            return Resource(Status.Error, null, message)
        }

        fun <T> loading(): Resource<T> {
            return Resource(Status.Loading)
        }
    }
}