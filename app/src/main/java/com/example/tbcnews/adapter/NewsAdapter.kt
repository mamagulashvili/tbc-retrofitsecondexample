package com.example.tbcnews.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.tbcnews.databinding.RowItemBinding
import com.example.tbcnews.model.News
import com.bumptech.glide.Glide

class NewsAdapter : RecyclerView.Adapter<NewsAdapter.NewsViewHolder>() {
    private val newsList = mutableListOf<News>()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder {
        return NewsViewHolder(
            RowItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        holder.onBind()
    }

    override fun getItemCount(): Int = newsList.size

    inner class NewsViewHolder(val binding: RowItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private lateinit var news:News
        fun onBind() {
            news = newsList[adapterPosition]
            binding.apply {
                tvNewsTitle.text = news.titleKA
                newsUpdatedAt.text = news.updatedAt.toString()
            }
            itemView.apply {
                Glide.with(this).load(news.cover).into(binding.ivCover)
            }
        }
    }
    fun setData(list:MutableList<News>){
        this.newsList.clear()
        this.newsList.addAll(list)
        notifyDataSetChanged()
    }
}