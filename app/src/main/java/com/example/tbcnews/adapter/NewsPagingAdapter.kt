package com.example.tbcnews.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.tbcnews.databinding.RowItemBinding
import com.example.tbcnews.model.News

class NewsPagingAdapter :
    PagingDataAdapter<News, NewsPagingAdapter.NewsViewHolder>(DiffUtilCallBack) {

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        holder.onBind()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder {
        return NewsViewHolder(
            RowItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    inner class NewsViewHolder(val binding: RowItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun onBind() {
            val news = getItem(bindingAdapterPosition)
            binding.apply {
                tvNewsTitle.text = news?.titleKA
                newsUpdatedAt.text = news?.updatedAt.toString()
            }
            itemView.apply {
                Glide.with(this).load(news?.cover).into(binding.ivCover)
            }
        }
    }

    object DiffUtilCallBack : DiffUtil.ItemCallback<News>() {
        override fun areItemsTheSame(oldItem: News, newItem: News): Boolean {

            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: News, newItem: News): Boolean {
            return oldItem == newItem
        }
    }


}