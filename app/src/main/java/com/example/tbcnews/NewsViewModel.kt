package com.example.tbcnews

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import androidx.paging.liveData
import com.example.tbcnews.model.News

class NewsViewModel : ViewModel() {


//    private val news = MutableLiveData<Resource<MutableList<News>>>().apply {
//        mutableListOf<News>()
//    }
    val newsList = Pager(PagingConfig(10)) {
        NewsDataResource()
    }.liveData.cachedIn(viewModelScope)


//    fun getNews() = viewModelScope.launch {
//        news.postValue(Resource.loading())
//        val response = RetrofitService.getRetrofitService().getNews()
//        if (response.isSuccessful) {
//            response.body()?.let {
//                Resource.success(it)
//            }
//        } else {
//            Resource.error(response.errorBody().toString())
//        }
//    }
}