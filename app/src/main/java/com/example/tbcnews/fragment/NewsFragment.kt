package com.example.tbcnews.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.tbcnews.NewsViewModel
import com.example.tbcnews.adapter.NewsPagingAdapter
import com.example.tbcnews.databinding.NewsFragmentBinding

class NewsFragment : Fragment() {

    private var _binding: NewsFragmentBinding? = null
    private val binding: NewsFragmentBinding get() = _binding!!

    private val newsViewModel: NewsViewModel by viewModels()
    private val newsAdapter: NewsPagingAdapter by lazy { NewsPagingAdapter() }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = NewsFragmentBinding.inflate(layoutInflater, container, false)
        init()
        return _binding?.root
    }

    private fun init() {
        setRecycle()
        newsViewModel.newsList.observe(viewLifecycleOwner, {
            newsAdapter.submitData(viewLifecycleOwner.lifecycle, it)
        })
        newsAdapter.addLoadStateListener { loadState ->
            if (loadState.refresh is LoadState.Loading) {
                showProgressBar()
            } else {
                hideProgressBar()
            }

        }
    }


//    private fun observeData() {
//        newsViewModel._news.observe(viewLifecycleOwner, {
//            when (it.status) {
//                Resource.Status.Success -> {
//                    it.data?.let { it -> newsAdapter.setData(it) }
//                    hideProgressBar()
//                }
//                Resource.Status.Error -> {
//                    d("Error Message", "${it.message}")
//                    hideProgressBar()
//                }
//                Resource.Status.Loading -> {
//                    showProgressBar()
//                }
//            }
//        })
//    }


    private fun showProgressBar() {
        binding.progressBar.visibility = View.VISIBLE
    }

    private fun hideProgressBar() {
        binding.progressBar.visibility = View.GONE
    }


    private fun setRecycle() {
        binding.rvNews.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = newsAdapter
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}