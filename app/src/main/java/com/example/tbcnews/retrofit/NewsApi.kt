package com.example.tbcnews.retrofit

import com.example.tbcnews.model.News
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET

interface NewsApi {
    @GET("/api/m/v2/news")
    suspend fun getNews():Response<MutableList<News>>
}