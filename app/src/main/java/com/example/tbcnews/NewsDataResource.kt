package com.example.tbcnews

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.example.tbcnews.model.News
import com.example.tbcnews.retrofit.RetrofitService

class NewsDataResource : PagingSource<Int, News>() {
    override fun getRefreshKey(state: PagingState<Int, News>): Int? {
        TODO("Not yet implemented")
    }

//    companion object {
//        val newsList = mutableListOf<News>()
//    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, News> {
        return try {
            val nextPage = params.key ?: 1
            val response = RetrofitService.getRetrofitService().getNews()
//            response.body()?.let { newsList.addAll(it.toMutableList()) }
            LoadResult.Page(
                data = response.body()!!,
                prevKey = if (nextPage == 1) null else nextPage - 1,
                nextKey = nextPage + 1
            )
        } catch (e: Exception) {
            LoadResult.Error(e)
        }
    }

}